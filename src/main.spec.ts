import { closestToZero } from "./main";

test("given an array of positive and negative integers, it returns the closest number to zero", () => {
  expect(closestToZero([8, 5, 10])).toBe(5);
  expect(closestToZero([5, 4, -9, 6, -10, -1, 8])).toBe(-1);
  expect(closestToZero([2, 0])).toBe(0);
});

test("if the closest number in input could be either negative or positive, the function returns the positive one", () => {
  expect(closestToZero([8, 2, 3, -2])).toBe(2);
  expect(closestToZero([8, -3, 2, 3, -2])).toBe(2);
});

test("if the input array is undefined or empty, the function returns 0", () => {
  expect(closestToZero(undefined)).toBe(0);
  expect(closestToZero([])).toBe(0);
});
