export function closestToZero(list: number[] = []): number {
  if (list === null) {
    return 0;
  }

  if (list.length === 0) {
    return 0;
  }

  const equalZero = (item: number) => item === 0;

  if (list.some(equalZero)) {
    return 0;
  }

  const testEquality = (previous: number, current: number) => {
    if (Math.abs(previous) === Math.abs(current)) {
      return Math.abs(previous);
    }

    if (Math.abs(previous) < Math.abs(current)) {
      return previous;
    }

    return current;
  };

  return list.reduce(testEquality);
}
